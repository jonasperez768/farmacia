export class Producto {
  _id? : number;

  producto : string;
  marca : string;
  tipo : string;
  cantidad : number;
  precio : number;

  constructor( producto: string, marca: string, tipo: string, cantidad: number, precio: number ){

    this.producto = producto;
    this.marca = marca;
    this.tipo = tipo;
    this.cantidad = cantidad;
    this.precio = precio;
  }





}
