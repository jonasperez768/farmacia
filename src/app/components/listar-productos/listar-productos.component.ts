import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/producto';
import { ProductoService } from 'src/app/services/producto.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-listar-productos',
  templateUrl: './listar-productos.component.html',
  styleUrls: ['./listar-productos.component.css']
})
export class ListarProductosComponent implements OnInit {
  listProductos: Producto[] = [];
  listProductosOriginal: Producto[] = [];
  filtro: string = '';

  constructor(private _productoService: ProductoService,
    private toastr: ToastrService){}

  ngOnInit(): void {
    this.obtenerProductos();
  }

  obtenerProductos(){
    this._productoService.getProductos().subscribe(data =>{
      //console.log(data);
      this.listProductos = data;
      this.listProductosOriginal = [...data];

    }, errror =>{
      console.log(errror);
    })
  }

  eliminarProducto(id: any){
    this._productoService.eliminarProducto(id).subscribe(data =>{
      this.toastr.error('El producto fue eliminado con exito', 'Producto Eliminado');
      this.obtenerProductos();

    }, error =>{
      console.log(error);
    })
  }

  filtrarProductos(): void {
    if (this.filtro) {
      this.listProductos = this.listProductos.filter(producto =>
        producto.producto.toLowerCase().includes(this.filtro.toLowerCase())

      );
    } else {
      // Si no hay término de búsqueda, muestra todos los productos
      this.listProductos = [...this.listProductosOriginal];
    }
  }





}
