import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductoService } from 'src/app/services/producto.service';
import { ToastrService } from 'ngx-toastr';
import { Producto } from 'src/app/models/producto';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-crear-producto',
  templateUrl: './crear-producto.component.html',
  styleUrls: ['./crear-producto.component.css']
})
export class CrearProductoComponent {
  productoForm: FormGroup;
  titulo = 'Crear producto';
  id: string | null;


  constructor(private fb: FormBuilder,
              private router: Router,
              private toastr: ToastrService,
              private _productoService: ProductoService,
              private aRouter: ActivatedRoute){
    this.productoForm = this.fb.group({
      producto: ['', Validators.required],
      marca: ['', Validators.required],
      tipo: ['', Validators.required],
      cantidad: ['', Validators.required],
      precio: ['', Validators.required],
    })
    this.id = this.aRouter.snapshot.paramMap.get('id');
   }

  ngOnInit(): void{
    this.esEditar();

 }

 agregarProducto() {


  const PRODUCTO: Producto ={
    producto: this.productoForm.get('producto')?.value,
    marca: this.productoForm.get('marca')?.value,
    tipo: this.productoForm.get('tipo')?.value,
    cantidad: this.productoForm.get('cantidad')?.value,
    precio: this.productoForm.get('precio')?.value
  }
  if(this.id ){
    // editamos producto

    this._productoService.editarProducto(this.id, PRODUCTO).subscribe(data =>{
      this.router.navigate(['/']);
    this.toastr.info('El producto fue actualizado con exito', 'Producto Actualizado!');
    },error =>{
      console.log(error);
      this.productoForm.reset()
    }
    )
  }else{
    // agregamos producto
    console.log(PRODUCTO);
    this._productoService.guardarproducto(PRODUCTO).subscribe(data =>{
    this.router.navigate(['/']);
    this.toastr.success('El producto fue registrado con exito', 'Producto Registrado!');


  },error =>{
    console.log(error);
    this.productoForm.reset()
  })


 }


 }

 esEditar() {

  if(this.id !== null){
    this.titulo = 'Editar producto';
    this._productoService.obtenerProducto(this.id).subscribe(data =>{
      this.productoForm.setValue({
        producto: data.producto,
        marca: data.marca,
        tipo: data.tipo,
        cantidad: data.cantidad,
        precio: data.precio
      })
    })
  }

 }

}
