import { Component } from '@angular/core';
import { Producto } from 'src/app/models/producto';
import { ProductoService } from 'src/app/services/producto.service';
@Component({
  selector: 'app-carrito-de-compras',
  templateUrl: './carrito-de-compras.component.html',
  styleUrls: ['./carrito-de-compras.component.css']
})
export class CarritoDeComprasComponent {
  listProductos: Producto[] = [];
  carrito: Producto[] = []; // Lista de productos en el carrito

  constructor(private _productoService: ProductoService) {}

  ngOnInit() {
    this.obtenerProductos();

  }
  obtenerProductos(){
    this._productoService.getProductos().subscribe(data =>{
      console.log(data);
      this.listProductos = data;

    }, errror =>{
      console.log(errror);
    })
  }

  eliminarProducto(producto: Producto) {
    // Lógica para eliminar el producto del carrito
    // Encuentra y elimina el producto del carrito
    const index = this.carrito.findIndex(p => p._id === producto._id);
  if (index !== -1) {
    const productoEnCarrito = this.carrito[index];
    if (productoEnCarrito.cantidad > 1) {
      productoEnCarrito.cantidad--;
    } else {
      this.carrito.splice(index, 1);
    }
  }
  }
  agregarProducto(producto: Producto) {
    // Lógica para agregar un producto al carrito
    const productoEnCarrito = this.carrito.find(p => p._id === producto._id);
  if (productoEnCarrito) {
    productoEnCarrito.cantidad++;
  } else {
    const nuevoProducto = { ...producto, cantidad: 1 };
    this.carrito.push(nuevoProducto);
  }


  }

  calcularSubtotal() {
    // Lógica para calcular el subtotal del carrito
    return this.carrito.reduce((total, producto) => total + (producto.precio * producto.cantidad), 0);
  }

  calcularTotal() {
    // Lógica para calcular el total del carrito
    const envio = 5; // Ejemplo: costo de envío fijo de $5
    return this.calcularSubtotal() + envio;
  }
}

